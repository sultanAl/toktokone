<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('services', function (Blueprint $table) {
            $table->increments('id');
            $table->text('item_description')->nullable();
            $table->string('from')->nullable();
            $table->string('to')->nullable();
            $table->integer('max_weight')->nullable();
            $table->string('service_time')->nullable();
            $table->integer('customer_id')->nullable();
            $table->integer('driver_id')->nullable();
            $table->integer('rating')->nullable();
            $table->enum('status', ['1','2','3','4','5','6'])->comment('1=Pending, 2=Declined, 3=Accepted,4=Delivered','5=Completed','Rejected')->default('1');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('services');
    }
}
