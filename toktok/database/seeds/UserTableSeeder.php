<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'name' => 'customer',
                'email' => 'customer@test.com',
                'password' => bcrypt('111111'),
                'first_name' => 'cus name',
                'last_name' => ' last cus',
                'phone_number' => '617-545-4254',
                'city' => 'Armonk Village',
                'zip_code' => '10504',
                'address' => '3512 Deans Lane',
                'state' => 'NY',
                'cin' => 'as',
                'dln' => 'ads',
                'rating'=>mt_rand(1, 5),
                'role' => 'customer',
                'date_of_birth' => Carbon\Carbon::now(),
                'created_at' => Carbon\Carbon::now(),
                'updated_at' => Carbon\Carbon::now(),
            ], [
                'name' => 'driver',
                'email' => 'driver@test.com',
                'password' => bcrypt('111111'),
                'first_name' => 'dre name',
                'last_name' => ' last ver',
                'phone_number' => '606-748-4678',
                'city' => 'Morehead',
                'zip_code' => '40351',
                'address' => '3302 North Bend River Road',
                'state' => 'KY',
                'cin' => '1234567',
                'dln' => '1234567',
                'rating'=>mt_rand(1, 5),
                'role' => 'driver',
                'date_of_birth' => Carbon\Carbon::now(),
                'created_at' => Carbon\Carbon::now(),
                'updated_at' => Carbon\Carbon::now(),
            ]

        ]);

    }
}
