<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('home');

});

Route::get('/run', function () {

    event(new \App\Events\NotificationEvent('wasim'));
    return 'success';
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/contact-us', 'HomeController@contactUs')->name('contact-us');
Route::get('/pusher/auth', 'HomeController@authPusher')->name('pusher.auth');


Route::get('message/{id}', 'MessagesController@Index')->name('message');
Route::get('message-store', 'MessagesController@store')->name('message.store');
Route::get('message-show', 'MessagesController@show')->name('message.show');

Route::get('/driver-login', 'DriversController@login')->name('driver.login');
Route::get('/driver-register', 'DriversController@register')->name('driver.register');
Route::get('/driver-availability', 'DriversController@availability')->name('driver.availability');
Route::get('/driver-availability-store', 'DriversController@availabilityStore')->name('driver.availability.store');
Route::get('/driver-invitation-list', 'DriversController@invitations')->name('driver.invitation.list');
Route::post('/post-rating', 'ServicesController@postRating')->name('post-rating');
Route::get('/customer-services', 'ServicesController@customerServices')->name('customer.services');
Route::get('/my-services-list', 'ServicesController@requestedService')->name('my.services.list');


Route::get('/notification/{id}', 'ServicesController@notificationToDriver')->name('notification');

Route::get('/find-driver', 'ServicesController@findDriver')->name('customer.services.store');
Route::get('/send-request/{id}', 'ServicesController@sendRequest')->name('send.request');
Route::get('/response/{service_id}/{status}', 'ServicesController@responseRequest')->name('response.request');

Route::get('chat/index', 'ChatController@getIndex');
Route::post('chat/auth', 'ChatController@postAuth');
Route::post('chat/message', 'ChatController@postMessage');
//Route::post('/file', 'ChatController@file');

Route::group(['prefix' => '/admin', 'middleware' => ['web']], function () {
    Route::get('/', function () {
        return view('admin.dashboard');


    });
});


