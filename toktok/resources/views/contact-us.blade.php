@extends('front-end.layouts.app')
@section('css')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.css" rel="stylesheet"/>
    @endsection
    @section('content')
            <!--Page Header-->
    <section class="page-header contactus_page">
        <div class="container">
            <div class="page-header_wrap">
                <div class="page-heading">
                    <br>
                    <h1>Contact Us</h1>
                </div>
                <ul class="coustom-breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li>Contact Us</li>
                </ul>
            </div>
        </div>
        <div class="dark-overlay"></div>
    </section>
    <section class="contact_us section-padding">
        <div class="container">
            <div  class="row">
                <div class="col-md-6">
                    <h3>Details</h3>
                    <div class="contact_form gray-bg">
                        <form action="{{route('driver.availability.store')}}" method="get">

                            {{--<input type="hidden" name="driver_id" value="{{auth()->user()->id}}">--}}
                            <div class="form-group">
                                <label class="control-label">Name <span>*</span></label>
                                <input type="text" name="name" id="from" class="form-control white_bg" required>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Email <span>*</span></label>
                                <input type="email" name="email" id="from" class="form-control white_bg" required>
                            </div>


                            <div class="form-group">
                                <label class="control-label">I am <span>*</span></label>
                                <select class="form-control white_bg" name="user_type">
                                    <option>Customer</option>
                                    <option>Driver</option>

                                </select>

                            </div>

                            <div class="form-group">
                                <label class="control-label">Message<span>*</span></label>

                                <textarea type="text" name="message" class="form-control white_bg" rows="4" cols="50" required>

                                    </textarea>
                            </div>

                            <div class="form-group">
                                <button class="btn" type="submit">Send<span class="angle_arrow"><i class="fa fa-angle-right" aria-hidden="true"></i></span></button>
                            </div>
                        </form>
                    </div>
                </div>
    </div>
    </div>
    </section>
        @endsection