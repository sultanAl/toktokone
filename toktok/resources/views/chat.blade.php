<!DOCTYPE html>
<html>
<head>
    <title>Real-Time Laravel with Pusher</title>
    <meta name="csrf-token" content="{{ csrf_token() }}" />

    {{--<a href="C:\xampp\htdocs\real-time-app\public\upload\Matric (Annual) Examination, 2016" download="proposed_file_name">Download</a>--}}


    <link href="//fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400,600,700,200italic,300italic" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="http://d3dhju7igb20wy.cloudfront.net/assets/0-4-0/all-the-things.css" />

    <style>
        .chat-app {
            margin: 50px;
            padding-top: 10px;
        }
        .chat-app .message:first-child {
            margin-top: 15px;
        }
        #messages {
            height: 300px;
            overflow: auto;
            padding-top: 5px;
        }

        #upload-file-container {
            background: url(/attach.png) no-repeat;
        }

        #upload-file-container input {
            filter: alpha(opacity=0);
            opacity: 0;
        }

    </style>

    <script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
    <script src="https://cdn.rawgit.com/samsonjs/strftime/master/strftime-min.js"></script>
    <script src="https://js.pusher.com/3.1/pusher.min.js"></script>

    <script>
        // Ensure CSRF token is sent with AJAX requests
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        // Added Pusher logging
        Pusher.log = function(msg) {
            console.log(msg);
        };
    </script>
</head>
<body>

<div class="stripe no-padding-bottom numbered-stripe">
    <div class="fixed wrapper">
        <ol class="strong" start="2">
            <li>
                <div class="hexagon"></div>
                <h2><b>Real-Time Chat</b> <small>Fundamental real-time communication.</small></h2>
            </li>
        </ol>
    </div>
</div>
{{--<a href="file:///C:/xampp/htdocs/real-time-app/public/attach.png" download>Download link here</a>--}}

<section class="blue-gradient-background">
    <div class="container">
        <div class="row light-grey-blue-background chat-app">

            <div id="messages">
                <div class="time-divide">
                    <span class="date">Today</span>
                </div>
            </div>

            {{--<div id="upload-file-container" >--}}
                {{--<input type="file" class="fa fa-paperclip"/>--}}
            {{--</div>--}}

            <div class="action-bar">
                <textarea class="input-message col-xs-10" placeholder="Your message"></textarea>

                {{--<div id="upload-file-container" class="option col-xs-1 white-background">--}}
                    {{--<input id="file" type="file"/>--}}
                {{--</div>--}}

                <form action="{{url('/file')}}" method="POST" enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                    <div id="upload-file-container" class="option col-xs-1 white-background">
                        <input id="file" type="file" name="file"  />
                    </div>
                    {{--<button type="submit" id="upload-button">Upload</button>--}}

                </form>

                <div class="option col-xs-1 green-background send-message">
                    <span class="white light fa fa-paper-plane-o"></span>
                </div>
            </div>

        </div>
    </div>
</section>

<script id="chat_message_template" type="text/template">
    <div class="message">
        <div class="avatar">
            <img src="">
        </div>
        <div class="text-display">
            <div class="message-data">
                <span class="author"></span>
                <span class="timestamp"></span>
                <span class="seen"></span>
            </div>
            <p class="message-body"></p>
        </div>
    </div>
</script>

<script>
    function init() {
        // send button click handling
        $('.send-message').click(sendMessage);
        $('.input-message').keypress(checkSend);
    }
    // Send on enter/return key
    function checkSend(e) {
        if (e.keyCode === 13) {
            return sendMessage();
        }
    }
    // Handle the send button being clicked
    function sendMessage() {
        var messageText = $('.input-message').val();
//        var file = document.getElementById('file');
//            uploadFile();

        if(messageText.length < 3) {
            return false;
        }
//        if (file) {
//            uploadFile();
//            var data = {chat_text:file};
//        }

//        else{
            var data = {chat_text: messageText};
//        }
        // Build POST data and make AJAX request
        $.post('/chat/message', data).success(sendMessageSuccess);
        // Ensure the normal browser event doesn't take place
        return false;
    }

    function uploadFile(){
        var $file = document.getElementById('file'),
                $formData = new FormData();

        if ($file.files.length > 0) {
            for (var i = 0; i < $file.files.length; i++) {
                $formData.append('file-' + i, $file.files[i]);
            }
        }
        var ajax = new XMLHttpRequest();
        ajax.open("POST", "/file",true);
        ajax.setRequestHeader("X-XSRF-TOKEN", "{{\Illuminate\Support\Facades\Crypt::encrypt(csrf_token())}}");
        ajax.send($formData);
    }
    // Handle the success callback
    function sendMessageSuccess() {
        $('.input-message').val('')
        console.log('message sent successfully');
    }
    // Build the UI for a new message and add to the DOM
    function addMessage(data) {
        // Create element from template and set values
        var el = createMessageEl();
        el.find('.message-body').html(data.text);
        el.find('.author').text(data.username);
//        el.find('.avatar img').attr('src', data.avatar)
        // Utility to build nicely formatted time
        el.find('.timestamp').text(strftime('%H:%M:%S %P', new Date(data.timestamp)));
        var messages = $('#messages');
        messages.append(el)
        // Make sure the incoming message is shown
        messages.scrollTop(messages[0].scrollHeight);
    }
    // Creates an activity element from the template
    function createMessageEl() {
        var text = $('#chat_message_template').text();
        var el = $(text);
        return el;
    }
    $(init);
    /***********************************************/
    var pusher = new Pusher('{{env("PUSHER_APP_KEY")}}', {
        authEndpoint: '/chat/auth',
        cluster: '{{ env('PUSHER_APP_CLUSTER') }}',
        auth: {
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        }
    });
    var channel = pusher.subscribe('{{$chatChannel}}');
    channel.bind('new-message', addMessage);
</script>

</body>
</html>