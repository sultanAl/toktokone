<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
<link rel="shortcut icon" type="image/x-icon" href="{{ asset('assets/images/tuk-tuk/logo.png')}}">
<title>Admin Tuk Tuk</title>
<link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700" rel="stylesheet">
<link href="/hrms/css/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="/hrms/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link href="/hrms/css/fullcalendar.min.css" rel="stylesheet" />
<link href="/hrms/css/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="/hrms/css/select2.min.css" type="text/css">
<link rel="stylesheet" href="/hrms/css/bootstrap-datetimepicker.min.css" type="text/css">
<link rel="stylesheet" href="/hrms/plugins/morris/morris.css">
<link href="/hrms/css/style.css" rel="stylesheet" type="text/css">
<!--[if lt IE 9]>
<script src="/hrms/js//html5shiv.min.js"></script>
<script src="/hrms/js/respond.min.js"></script>
<![endif]-->