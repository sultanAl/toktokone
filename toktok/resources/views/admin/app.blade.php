<!DOCTYPE html>
<html>
<head>
  @include('admin._partials._css')
    <style>
        @yield('css')
    </style>
</head>
<body class="theme-maroon">
<div class="main-wrapper">
    <div class="header">
        <div class="header-left">
            <a href="index.html" class="logo">
                <img src="/assets/images/tuk-tuk/logo.png" width="40" height="40" alt="">
            </a>
            <a href="index.html" class="logo-dark">
                <img src="/assets/images/tuk-tuk/logo.png" width="40" height="40" alt="">
            </a>
        </div>
        <div class="page-title-box pull-left">
            <h3>Tok Tok</h3>
        </div>
        <a id="mobile_btn" class="mobile_btn pull-left" href="#sidebar"><i class="fa fa-bars" aria-hidden="true"></i></a>
        <ul class="nav navbar-nav navbar-right user-menu pull-right">

            <li class="dropdown">
                <a href="profile.html" class="dropdown-toggle user-link" data-toggle="dropdown" title="Admin">
							<span class="user-img"><img class="img-circle" src="/hrms/images/user.jpg" width="40" alt="Admin">
							<span class="status online"></span></span>
                    <span>Admin</span>
                    <i class="caret"></i>
                </a>
                <ul class="dropdown-menu">
                    <li><a href="login.html">Logout</a></li>
                </ul>
            </li>
        </ul>
        <div class="dropdown mobile-user-menu pull-right">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-ellipsis-v"></i></a>
            <ul class="dropdown-menu pull-right">
                <li><a href="profile.html">My Profile</a></li>
                <li><a href="edit-profile.html">Edit Profile</a></li>
                <li><a href="settings.html">Settings</a></li>
                <li><a href="login.html">Logout</a></li>
            </ul>
        </div>
    </div>
    @include('admin._partials._sidebar')
    @yield('content')
</div>
@include('admin._partials._footer')
@yield('js')
</body>

</html>