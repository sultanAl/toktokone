@extends('front-end.layouts.app')
@section('css')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.css" rel="stylesheet"/>
<style>
    .chat
    {
        list-style: none;
        margin: 0;
        padding: 0;
    }

    .chat li
    {
        margin-bottom: 10px;
        padding-bottom: 5px;
        border-bottom: 1px dotted #B3A9A9;
    }

    .chat li.left .chat-body
    {
        margin-left: 60px;
    }

    .chat li.right .chat-body
    {
        margin-right: 60px;
    }


    .chat li .chat-body p
    {
        margin: 0;
        color: #777777;
    }

    .panel .slidedown .glyphicon, .chat .glyphicon
    {
        margin-right: 5px;
    }

    .panel-body
    {
        overflow-y: scroll;
        height: 250px;
    }

    ::-webkit-scrollbar-track
    {
        -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
        background-color: #F5F5F5;
    }

    ::-webkit-scrollbar
    {
        width: 12px;
        background-color: #F5F5F5;
    }

    ::-webkit-scrollbar-thumb
    {
        -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,.3);
        background-color: #555;
    }

</style>
@endsection
@section('content')
    <!--Page Header-->
    <section class="page-header contactus_page">
        <div class="container">
            <div class="page-header_wrap">
                <div class="page-heading">
                    <br>
                    <h1>Complete Conversation</h1>
                </div>
                <ul class="coustom-breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li>invitation</li>
                    <li>> message</li>
                </ul>
            </div>
        </div>
        <!-- Dark Overlay-->
        <div class="dark-overlay"></div>
    </section>
    <!-- /Page Header-->

    <!--Contact-us-->
    <section class="contact_us section-padding">
        <div class="container">
            <div  class="row">
                <div class="col-md-12">
                    <h3>Messages</h3>
                    <div class="contact_form gray-bg">
                        {{--@foreach($messages as $message)--}}
                            {{--<div class="form-group">--}}
                                {{--{{$message->message}}--}}
                            {{--</div>--}}
                        {{--@endforeach--}}

                                        <div class="panel-body" style="height: 60vh">
                                            <ul class="chat" id="chat_ul">
                                                @foreach($messages as $message)
                                                    @if($message->sender_id==auth()->user()->id)
                                                            <li class="right clearfix">
                                                        <span class="chat-img pull-right">
                                                         <img src="http://placehold.it/50/FA6F57/fff&amp;text={{substr($message->sender->first_name, 0, 2)}}" alt="User Avatar" class="img-circle">
                                                        </span>
                                                                <div class="chat-body clearfix">
                                                                    <div class="header">
                                                                        <small class=" text-muted"><span class="glyphicon glyphicon-time"></span>{{$message->created_at->diffForHumans()}}</small>
                                                                        <strong class="pull-right primary-font">{{@$message->sender->first_name}}</strong>
                                                                    </div>
                                                                    <p>
                                                                        {{$message->message}}
                                                                    </p>
                                                                </div>
                                                            </li>
                                                    @elseif($message->receiver_id==auth()->user()->id)
                                                            <li class="left clearfix">
                                                                <span class="chat-img pull-left">
                                                                    <img src="http://placehold.it/50/55C1E7/fff&amp;text={{substr($message->sender->first_name, 0, 2)}}" alt="User Avatar" class="img-circle">
                                                                 </span>
                                                                <div class="chat-body clearfix">
                                                                    <div class="header">
                                                                                <strong class="primary-font">{{@$message->sender->first_name}}</strong> <small class="pull-right text-muted">
                                                                                    <span class="glyphicon glyphicon-time"></span>{{$message->created_at->diffForHumans()}}</small>
                                                                    </div>
                                                                    <p> {{$message->message}}</p>
                                                                </div>
                                                            </li>
                                                    @endif
                                                @endforeach
                                            </ul>
                                        </div>
                                        <div class="panel-footer">
                                            <form action="{{route('message.store')}}" method="get">

                                            <input type="hidden" name="sender_id" value="{{auth()->user()->id}}">
                                            <input type="hidden" name="receiver_id" value="{{$id}}">

                                                <div class="input-group" style="background: #fff;">
                                                    <input name="message" id="btn-input" type="text" class="form-control input-sm" placeholder="Type your message here..." autofocus style="background: #fff;">
                                                    <span class="input-group-btn">
                                                        <button type="submit" class="btn btn-warning btn-sm" id="btn-chat">Send</button>
                                                     </span>
                                                </div>
                                            </form>
                                        </div>


                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- /Contact-us-->
    <script type="text/template" id="add_mxg">
        <li class="left clearfix">
            <span class="chat-img pull-left">
                <img id="img_text" src="http://placehold.it/50/55C1E7/fff&amp;text=w" alt="User Avatar" class="img-circle">
             </span>
            <div class="chat-body clearfix">
                <div class="header">
                    <strong class="primary-font" id="sender_name"></strong> <small class="pull-right text-muted">
                        <span class="glyphicon glyphicon-time" id="time_sending"></span></small>
                </div>
                <p id="message_id"></p>
            </div>
        </li>
    </script>
@endsection

@section('js')
    <script>
        var d = $('.panel-body');
        d.scrollTop(d.prop("scrollHeight"));
    </script>
    <script>
        window.receiverId = "{{auth()->user()?auth()->user()->id:''}}";
        window.senderId = "{{$id?$id:null}}";


        function createMxg(data) {
            var d = $('.panel-body');
            d.scrollTop(d.prop("scrollHeight"));
            console.log(data);
            var add_mxg = `
                  <li class="left clearfix">
            <span class="chat-img pull-left">
                <img id="img_text" src="http://placehold.it/50/55C1E7/fff&amp;text=`+data.sender_img+`" alt="User Avatar" class="img-circle">
             </span>
            <div class="chat-body clearfix">
                <div class="header">
                    <strong class="primary-font" id="sender_name">`+data.sender_name+`</strong> <small class="pull-right text-muted">
                        <span class="glyphicon glyphicon-time" id="time_sending">`+data.time+`</span></small>
                </div>
                <p id="message_id">`+data.message.message+`</p>
            </div>
        </li>
            `;

            $('#chat_ul').append(add_mxg);

        }
        // Subscribe to the channel we specified in our Laravel Event
        var channel2 = pusher.subscribe(window.senderId+'message-event-channel'+window.receiverId);
        // Bind a function to a Event (the full Laravel class)
        channel2.bind('App\\Events\\MessageEvent', createMxg);
    </script>

@endsection


