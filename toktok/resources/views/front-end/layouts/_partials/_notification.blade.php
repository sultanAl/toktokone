

    <a href="#notifications-panel" class="dropdown-toggle" data-toggle="dropdown">
        <i data-count="{{$services->count()}}" class="glyphicon glyphicon-bell notification-icon" style="color: #fff"></i>
    </a>

    <div class="dropdown-container">
        <div class="dropdown-toolbar">
            <div class="dropdown-toolbar-actions">
            </div>
            <h3 class="dropdown-toolbar-title">Notifications (<span class="notif-count">{{$services->count()}}</span>)</h3>
        </div>
        <ul class="dropdown-menu">
            @foreach($services as $service)
                <li class="notification active">
                    <div class="media">
                        <div class="media-left">
                            <div class="media-object">
                                <img src="http://placehold.it/50/FA6F57/fff&amp;text={{substr($service->customer->first_name, 0, 2)}}" alt="User Avatar" class="img-circle">

                            </div>
                        </div>
                        <div class="media-body">
                            <strong class="notification-title">{{$service->notificationDriver()}}</strong>
                            <p class="notification-desc">{{$service->item_description}}</p>
                            <div class="notification-meta">
                                <small class="timestamp">{{$service->service_time}}</small>
                            </div>
                        </div>
                    </div>
                </li>
            @endforeach

        </ul>
        <div class="dropdown-footer text-center">
            <a href="{{route('driver.invitation.list')}}">View All</a>
        </div>
    </div>

