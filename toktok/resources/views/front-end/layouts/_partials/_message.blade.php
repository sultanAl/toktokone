

<a href="#notifications-panel" class="dropdown-toggle" data-toggle="dropdown">
    <i data-count="{{$messages->count()}}" class="fa fa-comment notification-icon" style="color: #fff"></i>
</a>

<div class="dropdown-container">
    <div class="dropdown-toolbar">
        <div class="dropdown-toolbar-actions">
        </div>
        <h3 class="dropdown-toolbar-title">Messages (<span class="notif-count">{{$messages->count()}}</span>)</h3>
    </div>
    <ul class="dropdown-menu">
        @foreach($messages as $message)
            <li class="notification active">
                <a href="{{route('message',[$message->sender_id])}}" style="background:#f4f4f4">
                    <div class="media">
                        <div class="media-left">
                            <div class="media-object">
                                <img src="http://placehold.it/50/FA6F57/fff&amp;text={{substr($message->sender->first_name, 0, 2)}}" alt="User Avatar" class="img-circle">

                            </div>
                        </div>
                        <div class="media-body">
                            <strong class="notification-title" style="color: black">{{$message->sender->first_name}} has send message</strong>
                            <p class="notification-desc" style="color: black">{{$message->message}}</p>
                            <div class="notification-meta">
                                <small class="timestamp" style="color: black">{{$message->created_at->diffForHumans()}}</small>
                            </div>
                        </div>
                    </div>
                </a>
            </li>
        @endforeach

    </ul>
</div>

