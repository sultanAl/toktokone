<header>
    <!-- Navigation -->
    <nav id="navigation_bar" class="navbar navbar-inverse" data-spy="affix" style="z-index: 99999;width: 100%">
        {{--<nav class="navbar navbar-inverse" data-offset-top="197">--}}
        <div>
            <div class="logo" style="margin-top: 12px;margin-bottom: 19px;margin-left: 34px;}">
                <a href="{{route('home')}}">
                    <img src="{{ asset('assets/images/tuk-tuk/logo.png') }}" alt="image" style="    width: 60px;height: 40px;"></a> </div>
        </div>
            <div class="container">


            <div class="navbar-header">
                <button id="menu_slide" data-target="#navigation" aria-expanded="false" data-toggle="collapse" class="navbar-toggle collapsed" type="button">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <div class="header_wrap">
                @guest
                @else
                    <div class="alert_div" style="float: left;padding: 20px 0px 0px 0px;">
                        <ul>
                            <li class="dropdown dropdown-notifications" id="results_notification">
                                <a href="#notifications-panel" class="dropdown-toggle" data-toggle="dropdown">
                                    <i data-count="0" class="glyphicon glyphicon-bell notification-icon" style="color: #fff"></i>
                                </a>

                                <div class="dropdown-container">
                                    <div class="dropdown-toolbar">
                                        <div class="dropdown-toolbar-actions">
                                        </div>
                                        <h3 class="dropdown-toolbar-title">Notifications (<span class="notif-count">0</span>)</h3>
                                    </div>
                                    <ul class="dropdown-menu" id="results_notification">
                                    </ul>
                                    <div class="dropdown-footer text-center">
                                        <a href="{{route('driver.invitation.list')}}">View All</a>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div style="float: left;padding: 20px 15px 0px 0px;">
                        <ul>
                            <li class="dropdown dropdown-messages" id="results_message">
                                <a href="#notifications-panel" class="dropdown-toggle" data-toggle="dropdown">
                                    <i data-count="0" class="fa fa-comment notification-icon" style="color: #fff"></i>
                                </a>

                                <div class="dropdown-container">
                                    <div class="dropdown-toolbar">
                                        <div class="dropdown-toolbar-actions">
                                        </div>
                                        <h3 class="dropdown-toolbar-title">Message (<span class="notif-count">0</span>)</h3>
                                    </div>
                                    <ul class="dropdown-menu" >
                                    </ul>
                                </div>
                            </li>
                        </ul>

                    </div>
                @endguest


                <div class="user_login">

                    @guest
                    <ul>
                        <li class="dropdown">
                            <a href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fa fa-sign-in" aria-hidden="true"></i> login <i class="fa fa-angle-down" aria-hidden="true"></i></a>
                            <ul class="dropdown-menu">
                                <li><a href="{{ route('login') }}">Login Customer</a></li>
                                <li><a href="{{ route('register') }}">Register Customer</a></li>
                                <li><a href="{{ route('driver.login') }}">login driver</a></li>
                                <li><a href="{{ route('driver.register') }}">Register driver</a></li>
                            </ul>
                        </li>
                    </ul>
                    @else
                        <ul>
                            <li class="dropdown"> <a href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-user-circle" aria-hidden="true"></i> {{ @Auth::user()->name }} <i class="fa fa-angle-down" aria-hidden="true"></i></a>
                                <ul class="dropdown-menu">
                                    <li>
                                        <a href="{{ route('logout') }}"
                                           onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Sign Out
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                   <li>Account Details</li>
                                </ul>
                            </li>
                        </ul>
                    @endguest

                </div>

                <div class="header_search">
                    <div id="search_toggle"><i class="fa fa-search" aria-hidden="true"></i></div>
                    <form action="#" method="get" id="header-search-form">
                        <input type="text" placeholder="Search..." class="form-control">
                        <button type="submit"><i class="fa fa-search" aria-hidden="true"></i></button>
                    </form>
                </div>
            </div>
            <div class="collapse navbar-collapse" id="navigation">
                <ul class="nav navbar-nav">
                    <li><a href="{{route('home')}}">Home</a>
                    </li>

                    @guest
                    @else
                        @if(auth()->user()->role=='driver')
                            <li><a href="{{route('driver.availability')}}">Availability</a></li>
                            <li><a href="{{route('driver.invitation.list')}}">Invitations</a></li>

                        @else
                            <li><a href="{{route('customer.services')}}">Request For New Service</a></li>
                            <li><a href="{{route('my.services.list')}}">My Requested Services</a></li>


                        @endif

                            @endguest
                            <li><a href="{{route('contact-us')}}">Contact Us</a></li>
                </ul>
            </div>
        </div>


    </nav>
    <!-- Navigation end -->
</header>
