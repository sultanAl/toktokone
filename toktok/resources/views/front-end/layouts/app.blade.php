<!DOCTYPE HTML>
<html lang="en">
@include('front-end.layouts._partials._css-head')
<link href="{{ asset('/toastr/css/toastr.css') }}" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" type="text/css" href="{{ asset('bootstrap-notifications\dist\stylesheets\bootstrap-notifications.min.css') }}">
<link href="{{ asset('/date-picker/bootstrap-datepicker.min.css') }}" rel="stylesheet">
<link rel="stylesheet" href="//netdna.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">
<style>
    .btn.btn-xs {
        font-size: 11px;
        padding: 0px 8px;
    }
    .button-green{
        border-color: #079205 !important;
        color: #079205 !important;
        fill: #079205 !important;

    }
    .button-green:hover, .button-green:focus, .btn-link:hover {
        color: #fff !important;
        fill: #079205 !important;
        background: #079205 none repeat scroll 0 0 !important;
    }
    .button-blue{
        border-color: #002a80 !important;
        color: #002a80 !important;
        fill: #002a80 !important;

    }
    .button-blue:hover, .button-blur:focus, .btn-link:hover {
        color: #fff !important;
        fill: #002a80 !important;
        background: #002a80  none repeat scroll 0 0 !important;
    }
    h5,h4,h3,h2,h1{
        text-transform: uppercase;
    }
    div.stars {
        width: 270px;
        display: inline-block;
    }

    input.star { display: none; }

    label.star {
        float: right;
        padding: 10px;
        font-size: 36px;
        color: #444;
        transition: all .2s;
    }

    input.star:checked ~ label.star:before {
        content: '\f005';
        color: #FD4;
        transition: all .25s;
    }

    input.star-5:checked ~ label.star:before {
        color: #FE7;
        text-shadow: 0 0 20px #952;
    }

    input.star-1:checked ~ label.star:before { color: #F62; }

    label.star:hover { transform: rotate(-15deg) scale(1.3); }

    label.star:before {
        content: '\f006';
        font-family: FontAwesome;
    }
    .checked {
        color: orange;
    }

</style>
@yield('css')


<body>


<!--Header-->
@include('front-end.layouts._partials._nav-header')
<!-- /Header -->


@yield('content')

@include('front-end.layouts._partials._footer')

<!--Back to top-->
<div id="back-top" class="back-top"> <a href="#top"><i class="fa fa-angle-up" aria-hidden="true"></i> </a> </div>
<!--/Back to top-->

<!--Forgot-password-Form -->
<div class="modal fade" id="forgotpassword">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">

                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">Password Recovery</h3>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="forgotpassword_wrap">
                        <div class="col-md-12">
                            <form action="#" method="get">
                                <div class="form-group">
                                    <input type="email" class="form-control" placeholder="Your Email address*">
                                </div>
                                <div class="form-group">
                                    <input type="submit" value="Reset My Password" class="btn btn-block">
                                </div>
                            </form>
                            <div class="text-center">
                                <p class="gray_text">For security reasons we don't store your password. Your password will be reset and a new one will be send.</p>
                                <p><a href="#loginform" data-toggle="modal" data-dismiss="modal"><i class="fa fa-angle-double-left" aria-hidden="true"></i> Back to Login</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--/Forgot-password-Form -->

<!-- Scripts -->
<script src="{{url('/assets/js/jquery.min.js')}}"></script>
{{--<script src="https://js.pusher.com/4.2/pusher.min.js"></script>--}}
<script src="https://js.pusher.com/3.1/pusher.min.js"></script>
<script src="{{url('/assets/js/bootstrap.min.js')}}"></script>
<script src="{{url('/assets/js/interface.js')}}"></script>
<!--Switcher-->
<!--bootstrap-slider-JS-->
<script src="{{url('/assets/js/bootstrap-slider.min.js')}}"></script>
<!--Slider-JS-->
<script src="{{url('/assets/js/slick.min.js')}}"></script>
<script src="{{url('/assets/js/owl.carousel.min.js')}}"></script>

<script src="{{ asset('/toastr/js/toastr.min.js') }}"></script>
<script src="{{ asset('/toastr/js/pages/ui-toastr.js') }}"></script>

<script src="{{ asset('/date-picker/bootstrap-datepicker.min.js') }}"></script>
<script>
    @guest
        window.authId = '';
    @else
        window.authId = "{{auth()?auth()->user()->id:''}}";
        window.notif = "{{route('notification',[auth()->user()->id])}}";
    @endguest
</script>
<script>
    $( document ).ready(function() {
        if(window.authId) {

            $.ajax({
                url: window.notif,
                type: 'GET',
                headers: {'X-XSRF-TOKEN': '{{\Illuminate\Support\Facades\Crypt::encrypt(csrf_token())}}'},
                success: function ($data) {
                    $('#results_notification').html('')
                    $('#results_notification').append($data)
                }
            });
        }
    });
    $( document ).ready(function() {
        if(window.authId){
            $.ajax({
                url: '{{route('message.show')}}',
                type: 'GET',
                headers: {'X-XSRF-TOKEN': '{{\Illuminate\Support\Facades\Crypt::encrypt(csrf_token())}}'},
                success: function($data){
                    $('#results_message').html('')
                    $('#results_message').append($data)
                }
            });
        }

    });
</script>


<script>
    toastr.options = {
        "closeButton": true,
        "debug": false,
        "positionClass": "toast-bottom-right",
        "onclick": null,
        "showDuration": "1000",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "swing",
        "showMethod": "slideDown",
        "hideMethod": "fadeOut"
    };

    @if (Session::has('app_info'))
        toastr.info("{{ Session::get('app_info') }}", "Message");
    @endif

    @if (Session::has('app_message'))
toastr.success("{{ Session::get('app_message') }}", "Message");
    @endif

    @if (Session::has('app_warning'))
toastr.warning("{{ Session::get('app_warning') }}", "Message");
    @endif

    @if (Session::has('app_error'))
toastr.error("{{ Session::get('app_error') }}", "Message");
    @endif
</script>
<script type="text/javascript">
    var notificationsWrapper   = $('.dropdown-notifications');
    var notificationsToggle    = notificationsWrapper.find('a[data-toggle]');
    var notificationsCountElem = notificationsToggle.find('i[data-count]');
    var notificationsCount     = parseInt(notificationsCountElem.data('count'));
    var notifications          = notificationsWrapper.find('ul.dropdown-menu');

    if (notificationsCount <= 0) {
//        notificationsWrapper.hide();
    }

    var messagesWrapper   = $('.dropdown-messages');
    var messagesToggle    = messagesWrapper.find('a[data-toggle]');
    var messagesCountElem = messagesToggle.find('i[data-count]');
    var messagesCount     = parseInt(messagesCountElem.data('count'));
    var messages          = messagesWrapper.find('ul.dropdown-menu');

    if (messagesCount <= 0) {
//        messagesWrapper.hide();
    }

    function addNotification(data) {
        console.log(data);
        var existingNotifications = notifications.html();
        var avatar = Math.floor(Math.random() * (71 - 20 + 1)) + 20;
        var newNotificationHtml = `
          <li class="notification active">
              <div class="media">
                <div class="media-left">
                  <div class="media-object">
                    <img src="https://api.adorable.io/avatars/71/`+avatar+`.png" class="img-circle" alt="50x50" style="width: 50px; height: 50px;">
                  </div>
                </div>
                <div class="media-body">
                  <strong class="notification-title">`+data.message+`</strong>
                  <p class="notification-desc">`+data.service.item_description+`</p>
                  <div class="notification-meta">
                    <small class="timestamp">`+data.service.service_time +`</small>
                  </div>
                </div>
              </div>
          </li>
        `;
        notifications.html(newNotificationHtml + existingNotifications);

        notificationsCount += 1;
        notificationsCountElem.attr('data-count', notificationsCount);
        notificationsWrapper.find('.notif-count').text(notificationsCount);
        notificationsWrapper.show();
    }

    if(window.authId) {
        // Enable pusher logging - don't include this in production
        Pusher.logToConsole = true;

        var pusher = new Pusher('{{ env('PUSHER_APP_KEY') }}', {
            encrypted: false,
            cluster: '{{ env('PUSHER_APP_CLUSTER') }}',
            authEndpoint: '{{ route('pusher.auth') }}',
            auth: {
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                }
            }
        });

        // Subscribe to the channel we specified in our Laravel Event
        var channel = pusher.subscribe('notification-event-channel'+window.authId);
        // Bind a function to a Event (the full Laravel class)
        channel.bind('App\\Events\\NotificationEvent', addNotification);
    }
</script>


@yield('js')


</body>

</html>