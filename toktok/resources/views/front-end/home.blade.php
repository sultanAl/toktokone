@extends('front-end.layouts.app')
@section('css')
    <style>
        .banner_content h1 {
            color: #ffcc00;
            font-size: 26px;
            line-height: 35px;
        }
        .banner_content p{
        font-size: 13px;
        }
    </style>

    @endsection

@section('content')
<!-- Banners -->
<section id="banner" class="banner-section" style="padding: 122px 0;">
    <div class="container">
        <div class="div_zindex">
            <div class="row">
                <div class="col-md-5 col-md-push-8">
                    <div class="banner_content">
                        <h1>FIND THE RIGHT DRIVER FOR YOUR ITEM.</h1>
                        <p>We have more than a hundred drivers for you to choose. </p>
                        <a href="#" class="btn">Read More <span class="angle_arrow"><i class="fa fa-angle-right" aria-hidden="true"></i></span></a> </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- /Banners -->



<br>
<hr>

    @endsection

