@extends('front-end.layouts.app')
@section('css')
    <style>
        b{
            color: green;
        }
    </style>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.css" rel="stylesheet"/>
@endsection
@section('content')
    <!--Page Header-->
    <section class="page-header profile_page">
        <div class="container">
            <div class="page-header_wrap">
                <div class="page-heading">
                    <h1>Driver List</h1>
                </div>
                <ul class="coustom-breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li>Driver List</li>
                </ul>
            </div>
        </div>
        <!-- Dark Overlay-->
        <div class="dark-overlay"></div>
    </section>
    <!-- /Page Header-->

    <!--Dealers-list-->
    <section class="inner_pages">
        <div class="container">
            <div class="result-sorting-wrapper">
                <div class="sorting-count">
                    {{--<p>1 - 6 <span>of 50 Results for your search.</span></p>--}}
                </div>
                <div class="result-sorting-by">
                    <p>filter by Weight:</p>
                        <div class="form-group select sorting-select">
                            <select class="form-control result-sorting" id="filter-by-weight">
                                <option value="all">All</option>
                                <option value="5">1-5 lbs </option>
                                <option value="10">6-10 lbs </option>
                                <option value="20">11-20 lbs </option>
                                <option value="30">21-30 lbs </option>
                                <option value="50">30-50 lbs </option>
\                            </select>
                        </div>
                </div>
                <div class="result-sorting-by">
                    <p>sort by:</p>
                        <div class="form-group select sorting-select">
                            <select class="form-control result-sorting" id="sorting-by">
                                <option value="none">None</option>
                                <option value="rating">Rating</option>
                                <option value="near">Near By</option>
                                <option value="price">Low Price</option>
\                            </select>
                        </div>
                </div>

            </div>
            <div class="dealers_list_wrap" id="dealers_list_wrap">
                @include('front-end.services._partials._temp_driver_list')
            </div>
        </div>
    </section>
    <!--/Dealers-list-->
@endsection
@section('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.js" type="text/javascript" ></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>
    <script>
        $('.result-sorting').on('change', function() {
            var sorting_by = $('#sorting-by').val();
            var max_weight =  $('#filter-by-weight').val();


            $.ajax({
                url: '{{route('customer.services.store')}}',
                type: 'GET',
                data: {
                    search_weight : max_weight,
                    sort_by : sorting_by
                },
                headers: {'X-XSRF-TOKEN': '{{\Illuminate\Support\Facades\Crypt::encrypt(csrf_token())}}'},
                success: function($data){
                    $("#dealers_list_wrap").html('');
                    $("#dealers_list_wrap").append($data);
                }
            });
        })
    </script>

    {{--<script type="text/javascript">--}}
        {{--$(function () {--}}
            {{--$('#from,#to').datetimepicker({--}}
                {{--format: 'HH:mm'--}}
            {{--});--}}
        {{--});--}}
    {{--</script>--}}
@endsection

