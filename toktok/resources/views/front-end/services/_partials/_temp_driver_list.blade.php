    @foreach($drivers as $driver)
        <div class="dealers_listing">
            <div class="row">
                <div class="col-sm-3 col-xs-4">
                    <div class="dealer_logo"> <a href="#"><img src="assets/images/dealer-logo.jpg" alt="image"></a> </div>
                </div>
                <div class="col-sm-6 col-xs-8">
                    <div class="dealer_info">
                        <h5><a href="#">{{$driver->first_name}} {{$driver->last_name}}</a></h5>
                        <p><b>Address:</b>{{$driver->address}}<br>
                          <b>Service Area :</b> {{@$driver->availability()->first()->service_area}}, <br>
                            <b>Mobile No.</b> : {{@$driver->phone_number}}</p>
                        <p>
                            <b>Zip Code:</b> <label class="label label-success">{{$driver->zip_code}}</label> ,
                            &nbsp;&nbsp;&nbsp;
                            <b>Price Per Mile:</b> <label class="label label-info">$ {{@$driver->availability()->first()->charge}}</label>,
                            &nbsp;&nbsp;&nbsp;
                            <b>Rating:</b> <label class="label label-warning">{{$driver->rating}}</label></p>
                    </div>
                </div>
                <div class="col-sm-3 col-xs-12">
                    <div class="view_profile"> <a href="{{route('send.request',[$driver->id])}}" class="btn btn-xs outline">Send Request</a>
                        <a href="{{route('message',[$driver->id])}} " class="btn btn-xs outline button-blue">Message</a>
                    </div>
                </div>
            </div>
        </div>
    @endforeach

