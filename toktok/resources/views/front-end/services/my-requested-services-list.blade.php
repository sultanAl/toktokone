@extends('front-end.layouts.app')
@section('content')
        <!--Page Header-->
<section class="page-header contactus_page">
    <div class="container">
        <div class="page-header_wrap">
            <div class="page-heading">
                <br>
                <h1>My Requested Services list</h1>
            </div>
            <ul class="coustom-breadcrumb">
                {{--<li><a href="#">Home</a></li>--}}
                {{--<li>Request For Services</li>--}}
            </ul>
        </div>
    </div>
    <!-- Dark Overlay-->
    <div class="dark-overlay"></div>
</section>
<!-- /Page Header-->

<section class="inner_pages">
    <div class="container">
        <div class="dealers_list_wrap" id="dealers_list_wrap">
            @forelse($services as $service)
                <div class="dealers_listing">
                    <div class="row">
                        <div class="col-sm-3 col-xs-4">
                            <div class="dealer_logo"> <a href="#"><img src="assets/images/customer.jpg" alt="image"></a> </div>
                        </div>
                        <div class="col-sm-6 col-xs-8">
                            <div class="dealer_info">
                                <h5><a href="#">{{$service->driver->first_name}} {{$service->driver->last_name}}</a></h5>
                                <p>
                                    @if($service->status==1)
                                        Your services is pending, waiting for driver response
                                    @elseif($service->status==2)
                                        Unfortunately, at this time driver cannot provide the service you have requested.
                                    @elseif($service->status==3)
                                        Congrat! Your driver accepted your request, and will deliver on time.
                                    @elseif($service->status==4)
                                       Driver delivered your service. please Confirm or Reject.
                                    @elseif($service->status==5)
                                        This service is successfully completed.
                                        @else
                                        Admin will get more information from customer and driver, then will take a right decision.
                                    @endif
                                    <br>
                                    &nbsp;&nbsp;&nbsp;
                                    <b>Item Description:</b> <label class="label label-default">{{$service->item_description}}</label> ,

                                    <br>
                                    <b>from:</b> <label class="label label-default">{{$service->from}}</label> ,
                                    <b>to:</b> <label class="label label-default">{{$service->to}}</label> ,
                                </p>
                                <p>
                                    <b>Max Weight:</b> <label class="label label-success">{{$service->max_weight}} kg</label> ,
                                    &nbsp;&nbsp;&nbsp;
                                    <b>service_time:</b> <label class="label label-info"> {{$service->service_time}}</label>,
                                    &nbsp;&nbsp;&nbsp;
                                <p>{{$service->updated_at->diffForHumans()}}</p>
                            </div>
                        </div>
                        <div class="col-sm-3 col-xs-12">
                            @if($service->status==1)
                                <label class="label label-warning">Pending</label>
                                @elseif($service->status==2)
                                <label class="label label-info">Withdraw</label>
                            @elseif($service->status==3)
                                <label class="label label-default">Accepted</label>
                            @elseif($service->status==4)
                            <label class="label label-primary">Delivered</label>
                            <a href="" class="btn btn-xs outline button-green confirm" data-toggle="modal" data-target="#myModal" id="{{$service->id}}">Confirm</a>
                            <a href="{{route('response.request',[$service->id,6])}}" class="btn btn-xs outline">Reject</a>
                            @elseif($service->status==5)

                                <label class="label label-success">Completed</label>
                                {{--@for ($i = 0; $i < 10; $i++)--}}
                                   {{----}}
                                {{--@endfor--}}
                                @for ($i = 1; $i <= 5; $i++)
                                    @if($i<= $service->rating)
                                        <span class="fa fa-star checked"></span>
                                    @else
                                        <span class="fa fa-star"></span>
                                    @endif
                                @endfor
                                @else

                                <label class="label label-danger"> Dispute</label>
                                @endif

                        </div>
                    </div>
                </div>
            @empty
                <h3 class="text-center">You have not requested any service.</h3>
            @endforelse
                    <!-- Modal -->
                <div class="modal fade" id="myModal" role="dialog">
                    <div class="modal-dialog modal-sm">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Please Rate:</h4>
                            </div>
                            <div class="modal-body">
                                <div class="stars">

                                    <form action="{{ route('post-rating') }}" method="post">

                                        <input class="star star-5" id="star-5" type="radio" name="star" value="5"/>

                                        <label class="star star-5" for="star-5"></label>

                                        <input class="star star-4" id="star-4" type="radio" name="star" value="4"/>

                                        <label class="star star-4" for="star-4"></label>

                                        <input class="star star-3" id="star-3" type="radio" name="star" value="3"/>

                                        <label class="star star-3" for="star-3"></label>

                                        <input class="star star-2" id="star-2" type="radio" name="star" value="2"/>

                                        <label class="star star-2" for="star-2"></label>

                                        <input class="star star-1" id="star-1" type="radio" name="star" value="1"/>

                                        <label class="star star-1" for="star-1"></label>
                                        {{ csrf_field() }}
                                        <input type="hidden" name="service_id" id="service_id" value="">
                                        <input type="submit" value="Submit" class="btn btn-info">

                                    </form>

                                </div>

                            </div>

                        </div>
                    </div>
                </div>
        </div>


            {{--@include('front-end.driver.invitation._partials._temp_service_list')--}}
        </div>
    </div>

    </section>

<!--/Dealers-list-->
    @endsection
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
    $(document).ready(function(){
        $(".confirm").click(function(){
            var getID = $(this).closest('.confirm').attr('id');
$("#service_id").val( getID );
        });
    });
//    $(document).on("click", ".confirm", function () {
////$('.confirm' ).click(function() {
//    alert('hello')

//});


</script>