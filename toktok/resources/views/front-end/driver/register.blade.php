@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Register driver</div>

                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ route('register') }}">
                        {{ csrf_field() }}
                        <input  type="hidden" class="form-control" name="role" value="driver" required autofocus>
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">User Name</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="password" class="col-md-4 control-label">First Name</label>

                            <div class="col-md-6">
                                <input  type="text" class="form-control" name="first_name" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="password" class="col-md-4 control-label">Last Name</label>

                            <div class="col-md-6">
                                <input  type="text" class="form-control" name="last_name" required>
                            </div>
                        </div>


                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="phone_number" class="col-md-4 control-label">Phone Number</label>

                            <div class="col-md-6">
                                <input  type="number" class="form-control" name="phone_number" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="city" class="col-md-4 control-label">City</label>

                            <div class="col-md-6">
                                <select class="form-control" name="city">
                                    {{--<option> Abbeville</option>--}}
                                    {{--<option>  Aberdeen</option>--}}
                                    {{--<option>  Airmont</option>--}}
                                    <option> Austin</option>
                                </select>
                                {{--<input  type="text" class="form-control" name="city" required>--}}
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="zipcode" class="col-md-4 control-label">Zip Code</label>

                            <div class="col-md-6">
                                <input  type="number" class="form-control" name="zip_code" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="address" class="col-md-4 control-label">Address</label>

                            <div class="col-md-6">
                                <input  type="text" class="form-control" name="address" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="state" class="col-md-4 control-label">State</label>

                            <div class="col-md-6">
                                <select class="form-control" name="state" required>
                                    {{--<option> Louisiana</option>--}}
                                    {{--<option> Maryland</option>--}}
                                    {{--<option> New York</option>--}}

                                    <option>Taxes</option>
                                </select>
                                {{--<input  type="text" class="form-control" name="state" required>--}}
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="car's-insurance-Number" class="col-md-4 control-label">Car's Insurance Number</label>

                            <div class="col-md-6">
                                <input  type="text" class="form-control" name="cin" value="" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="pln" class="col-md-4 control-label">DLN</label>

                            <div class="col-md-6">
                                <input  type="text" class="form-control" name="dln" value="" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="pln" class="col-md-4 control-label">Date of Birth</label>

                            <div class="col-md-6">
                                <input  type="text" id="birthday" class="form-control" name="birthday" value="" required>
                            </div>
                        </div>
                        {{--<div class="form-group">--}}
                            {{--<label for="password" class="col-md-4 control-label">rating</label>--}}

                            {{--<div class="col-md-6">--}}
                                {{--<input  type="text" class="form-control" name="rating" value="" required>--}}
                            {{--</div>--}}
                        {{--</div>--}}

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Register
                                </button>
                            </div>
                        </div>
                        <div class="text-center">
                            <p>Already got an account? <a href="{{route('login')}}">Login Here</a></p>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('js')
    <script>
        $('#birthday').datepicker({
            autoclose:true,
            orientation: "bottom",
            format: "dd M yyyy",
            todayBtn: true,
            multidateSeparator: " ",
        })
    </script>
    @endsection
