@extends('front-end.layouts.app')
@section('css')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.css" rel="stylesheet"/>
@endsection
@section('content')
    <!--Page Header-->
    <section class="page-header contactus_page">
        <div class="container">
            <div class="page-header_wrap">
                <div class="page-heading">
                    <br>
                    <h1>Availability</h1>
                </div>
                <ul class="coustom-breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li>driver Availability</li>
                </ul>
            </div>
        </div>
        <!-- Dark Overlay-->
        <div class="dark-overlay"></div>
    </section>
    <!-- /Page Header-->

    <!--Contact-us-->
    <section class="contact_us section-padding">
        <div class="container">
            <div  class="row">
                <div class="col-md-6">
                    <h3>Please Set Your Availability Details</h3>
                    <div class="contact_form gray-bg">
                        <form action="{{route('driver.availability.store')}}" method="get">

                            <input type="hidden" name="driver_id" value="{{auth()->user()->id}}">
                            <div class="form-group">
                                <label class="control-label">From <span>*</span></label>
                                <input type="text" name="from" id="from" class="form-control white_bg" >
                            </div>
                            <div class="form-group">
                                <label class="control-label">To <span>*</span></label>
                                <input type="text" name="to" id="to" class="form-control white_bg" >
                            </div>

                            <div class="form-group">
                                <label class="control-label">Type of Vehicle <span>*</span></label>
                                <select class="form-control white_bg" name="type_of_vehicle">
                                    <option>  Sedan</option>
                                    <option> SUV</option>
                                </select>
                                {{--<input type="text" name="type_of_vehicle" class="form-control white_bg" >--}}
                            </div>
                            <div class="form-group">
                                <label for="max_weight" class="control-label">Maximum Weight<span>*</span></label>
                                <select id="max_weight" class="form-control white_bg" name="max_weight">
                                    <option value="5">1-5 Pound</option>
                                    <option value="10">6-10 Pound</option>
                                    <option value="20">11-20 Pound</option>
                                    <option value="30">21-30 Pound</option>
                                    <option value="50">30-50 Pound</option>
                                </select>
                                {{--<input type="text" name="max_weight" class="form-control white_bg" >--}}
                            </div>
                            <div class="form-group">
                                <label class="control-label">Charge per Pound<span>*</span></label>
                                <select class="form-control white_bg" name="charge">
                                    <option value="1">$1</option>
                                    <option value="2">$2</option>
                                    <option value="3">$3</option>
                                    <option value="5">$5</option>
                                    <option value="6">$6</option>
                                    <option value="7">$7</option>
                                    <option value="8">$8</option>
                                    <option value="9">$9</option>
                                    <option value="10">$10</option>
                                </select>
                                {{--<input type="text" name="charge" class="form-control white_bg" >--}}
                            </div>

                            <div class="form-group">
                                <label class="control-label">Zip Code<span>*</span></label>
                                <input type="text" name="zip_code" class="form-control white_bg" >
                            </div>

                            <div class="form-group">
                                <label class="control-label">Services Area<span>*</span></label>
                                <input type="text" name="service_area" class="form-control white_bg">
                            </div>

                            <div class="form-group">
                                <button class="btn" type="submit">Save Your Availability <span class="angle_arrow"><i class="fa fa-angle-right" aria-hidden="true"></i></span></button>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-md-6">
                    <h3>Contact Info</h3>
                    <div class="contact_detail">
                        <ul>
                            <li>
                                <div class="icon_wrap"><i class="fa fa-map-marker" aria-hidden="true"></i></div>
                                <div class="contact_info_m">3001 S Congress Ave, Austin, TX 78704</div>
                            </li>
                            <li>
                                <div class="icon_wrap"><i class="fa fa-phone" aria-hidden="true"></i></div>
                                <div class="contact_info_m"><a href="tel:61-1234-567-90">512-994-6754</a></div>
                            </li>
                            <li>
                                <div class="icon_wrap"><i class="fa fa-envelope-o" aria-hidden="true"></i></div>
                                <div class="contact_info_m"><a href="mailto:contact@exampleurl.com">contact@toktok.com</a></div>
                            </li>
                        </ul>
                        <div class="map_wrap">
                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d26361315.424069386!2d-113.75658747371207!3d36.241096924675375!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x54eab584e432360b%3A0x1c3bb99243deb742!2sUnited+States!5e0!3m2!1sen!2sin!4v1483614660041" width="100%" frameborder="0" style="border:0" allowfullscreen></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /Contact-us-->
@endsection
@section('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.js" type="text/javascript" ></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>
    <script type="text/javascript">
        $(function () {
            $('#from,#to').datetimepicker({
                format: 'HH:mm'
            });
        });
    </script>
@endsection

