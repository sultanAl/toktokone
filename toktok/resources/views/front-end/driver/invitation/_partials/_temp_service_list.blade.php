    @forelse($services as $service)
        <div class="dealers_listing">
            <div class="row">
                <div class="col-sm-3 col-xs-4">
                    <div class="dealer_logo"> <a href="#"><img src="assets/images/customer.jpg" alt="image"></a> </div>
                </div>
                <div class="col-sm-6 col-xs-8">
                    <div class="dealer_info">
                        <h5><a href="#">{{$service->customer->first_name}} {{$service->customer->last_name}}</a></h5>
                        <p>
{{--                        {{$service->customer->first_name}} {{$service->customer->last_name}} has sent you invitation.--}}
                            {{--&nbsp;&nbsp;&nbsp;--}}
                            <b>Item Description:</b> <label class="label label-default">{{$service->item_description}}</label> ,

                            <br>
                            <b>from:</b> <label class="label label-default">{{$service->from}}</label> ,
                            <b>to:</b> <label class="label label-default">{{$service->to}}</label> ,
                        </p>
                        <p>
                            <b>Max Weight:</b> <label class="label label-success">{{$service->max_weight}} kg</label> ,
                            &nbsp;&nbsp;&nbsp;
                            <b>service_time:</b> <label class="label label-info"> {{$service->service_time}}</label>,
                            &nbsp;&nbsp;&nbsp;
                        <p>{{$service->updated_at->diffForHumans()}}</p>
                    </div>
                </div>
                <div class="col-sm-3 col-xs-12">

                        @if($service->status==1)
                        {{--<label class="label label-warning">Pending</label>--}}
                     <a href="{{route('response.request',[$service->id,3])}}" class="btn btn-xs outline button-green">Accept Request</a>
                    <a href="{{route('response.request',[$service->id,2])}}" class="btn btn-xs outline">Decline</a>
                    <a href="{{route('message',[$service->customer_id])}}" class="btn btn-xs outline button-blue">Message</a>
                    @elseif($service->status==2)
                        <label class="label label-info">Declined</label>
                    @elseif($service->status==3)

                            <a href="{{route('response.request',[$service->id,4])}}" class="btn btn-xs outline button-green">Deliver</a>
                            <a href="{{route('message',[$service->customer_id])}}" class="btn btn-xs outline button-blue">Message</a>

                        @elseif($service->status==4)
                        <label class="label label-primary">Delivered</label>
                        <a href="{{route('message',[$service->customer_id])}}" class="btn btn-xs outline button-blue">Message</a>
                    @elseif($service->status==5)
                        <label class="label label-success">Completed</label>
                        @for ($i = 1; $i <= 5; $i++)
                            @if($i<= $service->rating)
                                <span class="fa fa-star checked"></span>
                            @else
                                <span class="fa fa-star"></span>
                            @endif
                        @endfor
                        @else
                        <label class="label label-danger"> Dispute</label>
                            @endif

                </div>
            </div>
        </div>
    @empty
        <h3 class="text-center">No invitation are Available</h3>
    @endforelse

