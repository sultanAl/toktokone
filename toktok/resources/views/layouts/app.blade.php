<!DOCTYPE HTML>
<html lang="en">
@include('layouts._partials._css-head')
<link href="{{ asset('/toastr/css/toastr.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('/front-end/date-picker/bootstrap-datepicker.min.css') }}" rel="stylesheet">
<style>
    .footer {

        position: fixed;
        margin-top: 15px;
        left: 0;
        bottom: 0;
        width: 100%;
        /*background-color: red;*/
        /*color: white;*/
        text-align: center;
        height: 80px;
    }

    .panel-default>.panel-heading{
        color: #fff;
        background: #fa2837;
        padding: 18px 15px;
    }
    .panel-default {
        border-color: #fa2837;
    }
</style>
@yield('css')

<body>


<!--Header-->
<header>
    <div class="default-header">
        <div class="container">
            <div class="row">
                <div class="col-sm-3 col-md-2">
                    <div class="logo"> <a href="{{route('home')}}"><img src="{{ asset('assets/images/tuk-tuk/logo.png') }}" alt="image" style="    width: 60px;height: 40px;"></a> </div>
                </div>
                <div class="col-sm-9 col-md-10">
                    <div class="header_info">
                        <div class="login_btn"> <a href="{{route('login')}}" class="btn btn-xs uppercase" data-toggle="modal" data-dismiss="modal">customer Login / Register</a> </div>
                    </div>
                    <div class="header_info">
                        <div class="login_btn"> <a href="{{route('driver.login')}}" class="btn btn-xs uppercase" data-toggle="modal" data-dismiss="modal">driver Login / Register</a> </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header><!-- /Header -->
<div style="margin-top: 50px;margin-bottom: 95px">
    @yield('content')

</div>

<!--Footer -->
<footer class="footer">
    <div class="footer-bottom">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-push-6 text-right">
                    <div class="footer_widget">
                        <p>Download Our APP:</p>
                        <ul>
                            <li><a href="#"><i class="fa fa-android" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-apple" aria-hidden="true"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-6 col-md-pull-6">
                    <p class="copy-right">Copyright &copy; 2017 CarForYou. All Rights Reserved</p>
                </div>
            </div>
        </div>
    </div>
</footer>

<!-- /Footer-->

<!--Back to top-->
<div id="back-top" class="back-top"> <a href="#top"><i class="fa fa-angle-up" aria-hidden="true"></i> </a> </div>
<!--/Back to top-->

<!--Login-Form -->
<div class="modal fade" id="loginform">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">Login</h3>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="login_wrap">
                        <div class="col-md-6 col-sm-6">
                            <form action="#" method="get">
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Username or Email address*">
                                </div>
                                <div class="form-group">
                                    <input type="password" class="form-control" placeholder="Password*">
                                </div>
                                <div class="form-group checkbox">
                                    <input type="checkbox" id="remember">
                                    <label for="remember">Remember Me</label>
                                </div>
                                <div class="form-group">
                                    <input type="submit" value="Login" class="btn btn-block">
                                </div>
                            </form>
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <h6 class="gray_text">Login the Quick Way</h6>
                            <a href="#" class="btn btn-block facebook-btn"><i class="fa fa-facebook-square" aria-hidden="true"></i> Login with Facebook</a> <a href="#" class="btn btn-block twitter-btn"><i class="fa fa-twitter-square" aria-hidden="true"></i> Login with Twitter</a> <a href="#" class="btn btn-block googleplus-btn"><i class="fa fa-google-plus-square" aria-hidden="true"></i> Login with Google+</a> </div>
                        <div class="mid_divider"></div>
                    </div>
                </div>
            </div>
            <div class="modal-footer text-center">
                <p>Don't have an account? <a href="#signupform" data-toggle="modal" data-dismiss="modal">Signup Here</a></p>
                <p><a href="#forgotpassword" data-toggle="modal" data-dismiss="modal">Forgot Password ?</a></p>
            </div>
        </div>
    </div>
</div>
<!--/Login-Form -->

<!--Register-Form -->
<div class="modal fade" id="signupform">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">Sign Up</h3>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="signup_wrap">
                        <div class="col-md-6 col-sm-6">
                            <form action="#" method="get">
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Full Name">
                                </div>
                                <div class="form-group">
                                    <input type="email" class="form-control" placeholder="Email Address">
                                </div>
                                <div class="form-group">
                                    <input type="password" class="form-control" placeholder="Password">
                                </div>
                                <div class="form-group">
                                    <input type="password" class="form-control" placeholder="Confirm Password">
                                </div>
                                <div class="form-group checkbox">
                                    <input type="checkbox" id="terms_agree">
                                    <label for="terms_agree">I Agree with <a href="#">Terms and Conditions</a></label>
                                </div>
                                <div class="form-group">
                                    <input type="submit" value="Sign Up" class="btn btn-block">
                                </div>
                            </form>
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <h6 class="gray_text">Login the Quick Way</h6>
                            <a href="#" class="btn btn-block facebook-btn"><i class="fa fa-facebook-square" aria-hidden="true"></i> Login with Facebook</a> <a href="#" class="btn btn-block twitter-btn"><i class="fa fa-twitter-square" aria-hidden="true"></i> Login with Twitter</a> <a href="#" class="btn btn-block googleplus-btn"><i class="fa fa-google-plus-square" aria-hidden="true"></i> Login with Google+</a> </div>
                        <div class="mid_divider"></div>
                    </div>
                </div>
            </div>
            <div class="modal-footer text-center">
                <p>Already got an account? <a href="#loginform" data-toggle="modal" data-dismiss="modal">Login Here</a></p>
            </div>
        </div>
    </div>
</div>
<!--/Register-Form -->

<!--Forgot-password-Form -->
<div class="modal fade" id="forgotpassword">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">

                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">Password Recovery</h3>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="forgotpassword_wrap">
                        <div class="col-md-12">
                            <form action="#" method="get">
                                <div class="form-group">
                                    <input type="email" class="form-control" placeholder="Your Email address*">
                                </div>
                                <div class="form-group">
                                    <input type="submit" value="Reset My Password" class="btn btn-block">
                                </div>
                            </form>
                            <div class="text-center">
                                <p class="gray_text">For security reasons we don't store your password. Your password will be reset and a new one will be send.</p>
                                <p><a href="#loginform" data-toggle="modal" data-dismiss="modal"><i class="fa fa-angle-double-left" aria-hidden="true"></i> Back to Login</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--/Forgot-password-Form -->

<!-- Scripts -->
<script src="{{url('/assets/js/jquery.min.js')}}"></script>
<script src="{{url('/assets/js/bootstrap.min.js')}}"></script>
<script src="{{url('/assets/js/interface.js')}}"></script>
<!--Switcher-->
<!--bootstrap-slider-JS-->
<script src="{{url('/assets/js/bootstrap-slider.min.js')}}"></script>
<!--Slider-JS-->
<script src="{{url('/assets/js/slick.min.js')}}"></script>
<script src="{{url('/assets/js/owl.carousel.min.js')}}"></script>

<script src="{{ asset('/toastr/js/toastr.min.js') }}"></script>
<script src="{{ asset('/toastr/js/pages/ui-toastr.js') }}"></script>
<script src="{{ asset('/date-picker/bootstrap-datepicker.min.js') }}"></script>


<script>

    toastr.options = {
        "closeButton": true,
        "debug": false,
        "positionClass": "toast-bottom-right",
        "onclick": null,
        "showDuration": "1000",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "swing",
        "showMethod": "slideDown",
        "hideMethod": "fadeOut"
    };

    @if (Session::has('app_info'))
        toastr.info("{{ Session::get('app_info') }}", "Message");
    @endif

    @if (Session::has('app_message'))
toastr.success("{{ Session::get('app_message') }}", "Message");
    @endif

    @if (Session::has('app_warning'))
toastr.warning("{{ Session::get('app_warning') }}", "Message");
    @endif

    @if (Session::has('app_error'))
toastr.error("{{ Session::get('app_error') }}", "Message");
    @endif

</script>


@yield('js')


</body>

</html>