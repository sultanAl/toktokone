<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class MessageEvent implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;


    public $sender;
    public $message;
    public $receiver;
    public $sender_name;
    public $time;
    public $sender_img;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($message,$sender,$receiver)
    {
        $this->sender = $sender;
        $this->message = $message;
        $this->receiver = $receiver;
        $this->sender_name=$message->sender->first_name;
        $this->time=$message->created_at->diffForHumans();
        $this->sender_img = substr($message->sender->first_name, 0, 2);
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return [$this->sender.'message-event-channel'.$this->receiver];
    }
}
