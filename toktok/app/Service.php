<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    protected $guarded=[];

    public function driver()
    {
        return $this->belongsTo(User::class,'driver_id');
    }
    public function customer()
    {
        return $this->belongsTo(User::class,'customer_id');
    }
    public function notificationDriver(){
        if(auth()->user()->role=='driver'){
            $message = $this->customer->first_name.' '.$this->customer->last_name .' has send invitation';
        }else{
            $status = $this->status;
            if($status==1){
                $value = 'Pending';
            }if($status==2){
                $value = 'Declined';
            }if($status==3){
                $value = 'Accepted';
            }if($status==4){
                $value = 'Completed';
            }
            $message = $this->driver->first_name.' '.$this->driver->last_name .' has '. $value . ' your invitation';
        }
        return $message;
    }

}
