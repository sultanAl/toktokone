<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

class ChatController extends Controller
{
    var $pusher;
    var $user;
    var $chatChannel;
    const DEFAULT_CHAT_CHANNEL = 'private-chat';

    public function __construct()
    {
        $this->pusher = new \Pusher\Pusher(env('PUSHER_APP_KEY'), env('PUSHER_APP_SECRET'), env('PUSHER_APP_ID'),array('cluster' => env('PUSHER_APP_CLUSTER')));
        $this->chatChannel = self::DEFAULT_CHAT_CHANNEL;
    }

    /**
     * Serve up the chat example app. Redirect if the user is not logged in.
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function getIndex()
    {
        return view('chat', ['chatChannel' => $this->chatChannel]);
    }

    /**
     * Handle a chat message POST request
     *
     * @param Request $request
     */
    public function postMessage(Request $request)
    {
        $message = [
            'text' => e($request->input('chat_text')),
            'username' => auth()->user()->name,
//            'avatar' => $this->user->getAvatar(),
            'timestamp' => (time() * 1000)
        ];

        $this->pusher->trigger($this->chatChannel, 'new-message', $message);
    }

    /**
     * Authenticate a subscription request.
     *
     * @param Request $request
     * @return Response
     */
    public function postAuth(Request $request)
    {
        $this->user = auth()->user();

        if ($this->user) {
            // TODO: should check if the $channelName has a 'private-' prefix.
            $channelName = $request->input('channel_name');
            $socketId = $request->input('socket_id');
            $auth = $this->pusher->socket_auth($channelName, $socketId);

            return $auth;
        } else {
            return (new Response('Not Authorized', 401));
        }
    }

    /**
     * @param Request $request
     */
    public function file(Request $request)
    {
        $file = $request['file-0'];
        if ($file) {
            $filename = $file->getClientOriginalName();
            $destinationPath = (public_path() . '\upload');
            $file->move($destinationPath, $filename);
        }
    }
}