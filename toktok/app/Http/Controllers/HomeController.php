<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('front-end.home');
    }
    public function contactUs()
    {
        return view('contact-us');
    }

    protected function authPusher(Request $request)
    {
        if (auth()->user()) {

            $pusher = new \Pusher\Pusher(env('PUSHER_APP_KEY'), env('PUSHER_APP_SECRET'), env('PUSHER_APP_ID'), array('cluster' => env('PUSHER_APP_CLUSTER')));

            // TODO: should check if the $channelName has a 'private-' prefix.
            $channelName = $request->input('channel_name');
            $socketId = $request->input('socket_id');
            $auth = $pusher->socket_auth($channelName, $socketId);

            return $auth;
        } else {
            return (new Response('Not Authorized', 401));
        }
    }

}
