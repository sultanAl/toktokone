<?php

namespace App\Http\Controllers;

use App\Service;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class ServicesController extends Controller
{
    public function customerServices()
    {
                Session::forget('service');
        return view('front-end.services.services');
    }

    public function findDriver(Request $request)
    {
        $service = $request->all();
        Session::push('service', $service);
        $query = User::query()->where('role', 'driver');

        if ($request->search_weight) {

            if($request->search_weight != 'all') {
                $query = $query->whereHas('availability', function ($q) use ($request) {
                    return $q->where('max_weight', $request->search_weight);
                });
            }
            if($request->sort_by != 'none') {
                if ($request->sort_by == 'rating'){
                   $query = $query->orderByDesc('rating');
                }
                if ($request->sort_by == 'near'){
                    $query = $query->whereHas('availability', function ($q) use ($request) {
                        return $q->where('service_area','like','%'.$request->from.'%');

                    });
                }
                if ($request->sort_by == 'price'){
                    $query = $query->whereHas('availability', function ($q) use ($request) {
                        return $q->orderBy('charge');
                    });
                }

            }

            $drivers = $query->paginate(5);

            return view('front-end.services._partials._temp_driver_list', compact('service', 'drivers'));
        }

        $drivers = $query->paginate(5);

        return view('front-end.services.send-request', compact('service', 'drivers'));
    }

    public function sendRequest(Request $request, $id)
    {
        $service = Session::get('service');

        $service[0]['driver_id'] = $id;
        $service = Service::create($service[0]);
        $receiver = $service->driver_id;
        $message = $service->customer->first_name.' '.$service->customer->last_name .' has send invitation';
        event(new \App\Events\NotificationEvent($service,$message,$receiver));
//        Session::forget('service');
        session()->flash('app_message', 'Send Email To Driver Successfully');
        return redirect()->route('customer.services.store');
    }
    public function responseRequest($service_id,$status)
    {
        $service = Service::where('id',$service_id)->first();
        $service->update([
           'status'=>$status
        ]);
        if($status==1){
            $value = 'Pending';
        }if($status==2){
            $value = 'Declined';
        }if($status==3){
            $value = 'Accepted';
        }if($status==4){
            $value = 'Delivered';
        }
        if($status==5){
            $value = 'Completed';
        }
        if($status==6){
            $value = 'Dispute';
        }
        $receiver = $service->customer_id;
        $message = $service->driver->first_name.' '.$service->driver->last_name .' has '. $value . ' your invitation';
        event(new \App\Events\NotificationEvent($service,$message,$receiver));
        session()->flash('app_message', 'Send Email To customer Successfully');
        return redirect()->back();
    }
    public function requestedService(){
        $services = Service::query()
            ->where('customer_id',auth()->user()->id)->orderByDesc('created_at')->get();
        return view('front-end.services.my-requested-services-list',compact('services'));
    }
    public function notificationToDriver($id){
        if(auth()->user()->role=='driver'){
            $services = Service::where('driver_id',$id)->where('status',1)->get();
        }else{
            $services = Service::where('customer_id',$id)->where('status','!=',1)->get();
        }

        return view('front-end.layouts._partials._notification',compact('services'));
    }
    public function postRating(Request $request)
    {
//        dd($request['service_id']);
        $service = Service::where('id',$request['service_id'])->first();
        $service->update([
            'status'=>'5',
            'rating'=>$request['star']
        ]);
        //dd($request->all());
//        return 'ali';
        return redirect()->back();
    }
}
