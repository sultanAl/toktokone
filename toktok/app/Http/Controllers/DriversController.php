<?php

namespace App\Http\Controllers;

use App\Availability;
use App\Service;
use Illuminate\Http\Request;

class DriversController extends Controller
{
    public function register(){
        session()->flash('app_message', 'Register as Driver');
        return view('front-end.driver.register');
    }
    public function login(){
        session()->flash('app_message', 'Login as Driver');
        return view('front-end.driver.login');
    }
    public function availability(){
        return view('front-end.driver.availability');
    }
    public function availabilityStore(Request $request){
        Availability::create($request->all());
        session()->flash('app_message', 'Created Successfully');
        return redirect()->route('home');
    }
    public function invitations(){
        $services = Service::query()/*->where('status','1')*/
            ->where('driver_id',auth()->user()->id)->orderByDesc('created_at')->get();
    return view('front-end.driver.invitation.index',compact('services'));
    }
}
