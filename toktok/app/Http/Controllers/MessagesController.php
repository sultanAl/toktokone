<?php

namespace App\Http\Controllers;

use App\Message;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class MessagesController extends Controller
{
    public function index($id){
        $messages = Message::query()->orderBy('created_at')->get();
        return view('front-end.message.message',compact('messages','id'));
    }
    public function store(Request $request){

        $message = Message::create($request->all());

        event(new \App\Events\MessageEvent($message,$message->sender_id,$message->receiver_id));

        $id = $request->receiver_id;

        return redirect()->route('message',[$id]);
    }
    public function show(){
        $messages = Message::query()->where('receiver_id',auth()->user()->id)->orderByDesc('created_at')->get();
        return view('front-end.layouts._partials._message',compact('messages'));

    }
}
